# Feature Highlights

LFortran is in development, there are features that work today, and there are
features that are being implemented.

## Works today

* **Full Fortran 2018 parser**  
    The LFortran's parser should now be able to parse any Fortran 2018 syntax
    to AST (Abstract Syntax Tree) and format it back as Fortran source code
    (`lfortran fmt`).

* **Interactive, Jupyter support**  
    LFortran can be used as a Jupyter kernel, allowing Python/Julia-style rapid
    prototyping and an exploratory workflow (`conda install jupyter lfortran`).
    It can also be used from the command-line with an interactive prompt
    (REPL).

* **Clean, modular design, usable as a library**  
    LFortran is structured around two independent modules, AST (Abstract Syntax
    Tree) and ASR (Abstract Semantic Representation), both of which are
    standalone (completely independent of the rest of LFortran) and users are
    encouraged to use them independently for other applications and build tools
    on top. See the [Design](https://docs.lfortran.org/design/) and
    [Developer Tutorial](https://docs.lfortran.org/developer_tutorial/) documents for
    more details.

* **Initial interoperation with GFortran**  
    LFortran has a prototype parser for GFortran module files and generation of
    a Fortran wrapper that allows using the module with any Fortran compiler.
    This feature is still under development.

* **Create executables**  
    It can create executables just like other Fortran compilers.

* **Runs on Linux, Mac and Windows**  
    All three platforms are regularly tested by our CI.

* **Several backends**
    The LLVM can be used to compile to binaries and for interactive usage. The
    C++ backend translates Fortran code to a readable C++ code. The x86 backend
    allows very fast compilation directly to x86 machine code.


## Under Development

These features are under development:

* **Full Fortran 2018 support**  
    The parser can now parse all of Fortran 2018 syntax to AST. A smaller
    subset can be transformed into ASR and even smaller subset compiled via
    LLVM to machine code. We are now working on extending the subset that
    LFortran can fully compile until we reach full Fortran 2018 support.

* **Native interoperation with other languages (and other Fortran compilers)**  
    Import libraries written in other languages (such as C or Python) with the
    `use` statement, see [#44]. Add support for module files generated with
    other Fortran compilers, see [#56]. This will allow LFortran to run a large
    array of production codes. Provide automatic wrappers of any Fortran module
    to other languages.

* **Support for diverse hardware**  
    LLVM makes it possible to run LFortran on diverse hardware and take
    advantage of native Fortran language constructs (such as `do concurrent`)
    on multi-core CPUs and GPUs, see [#57].

Please vote on issues in our [issue tracker] that you want us to prioritize
(feel free to create new ones if we are missing anything).
You can follow the [issue boards] to see what issues we are currently working
on.


[static]: https://nbviewer.jupyter.org/gist/certik/f1d28a486510810d824869ab0c491b1c
[interactive]: https://mybinder.org/v2/gl/lfortran%2Fweb%2Flfortran-binder/master?filepath=Demo.ipynb

[issue tracker]: https://gitlab.com/lfortran/lfortran/-/issues
[issue boards]: https://gitlab.com/lfortran/lfortran/-/boards
[#12]: https://gitlab.com/lfortran/lfortran/issues/12 'Implement "Language Service"'
[#29]: https://gitlab.com/lfortran/lfortran/issues/29 "Potential features"
[#44]: https://gitlab.com/lfortran/lfortran/issues/44 "Bridge to Python and other languages"
[#52]: https://gitlab.com/lfortran/lfortran/issues/52 'Allow to "use" modules compiled by gfortran'
[#56]: https://gitlab.com/lfortran/lfortran/issues/56 "Add support for other Fortran compilers"
[#57]: https://gitlab.com/lfortran/lfortran/issues/57 "Modern architectures support"
[#58]: https://gitlab.com/lfortran/lfortran/issues/58 "Support the latest Fortran 2018 standard"
[#70]: https://gitlab.com/lfortran/lfortran/issues/70 "Port to C++"
[#71]: https://gitlab.com/lfortran/lfortran/issues/71 "SymPy integration"
[#72]: https://gitlab.com/lfortran/lfortran/issues/72 "Rewrite code to an older Fortran standard"
[#73]: https://gitlab.com/lfortran/lfortran/issues/73 "Implement Fortran doctest feature"
[#74]: https://gitlab.com/lfortran/lfortran/issues/74 "Port all of LFortran to use ASR"
[#92]: https://gitlab.com/lfortran/lfortran/issues/92 "Document what subset of Fortran is currently supported"
